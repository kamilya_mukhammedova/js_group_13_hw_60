import { EventEmitter } from '@angular/core';

export class RouletteService {
  newNumber = new EventEmitter<number[]>();
  interval = 0;
  private numbersArray: number[] = [];

  getNumbers() {
    return this.numbersArray.slice();
  }

  generateNumber() {
    const randomNumber = Math.floor(Math.random() * 37);
    this.numbersArray.push(randomNumber);
    this.newNumber.emit(this.numbersArray);
  }

  start() {
    if(this.interval) return;
    this.interval = setInterval(() => {
      this.generateNumber();
    }, 1000);
  }

  stop() {
    clearInterval(this.interval);
    this.interval = 0;
  }

  getColor(number: number) {
    if((number >= 1 && number <= 10) || (number >= 19 && number <= 28)) {
      if(number % 2 === 0) {
        return 'black';
      } else {
        return 'red';
      }
    } else if((number >= 11 && number <= 18) || (number >= 29 && number <= 36)) {
      if(number % 2 === 0) {
        return 'red';
      } else {
        return 'black';
      }
    } else if(number === 0) {
      return 'zero';
    } else {
      return 'unknown';
    }
  }

  reset() {
    this.numbersArray.length = 0;
    clearInterval(this.interval);
    this.interval = 0;
  }
}
