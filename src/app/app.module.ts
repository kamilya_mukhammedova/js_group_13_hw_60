import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RouletteService } from '../shared/roulette.service';
import { ColorChangeDirective } from './directives/color-change.directive';

@NgModule({
  declarations: [
    AppComponent,
    ColorChangeDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [RouletteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
